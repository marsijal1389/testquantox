<?php

class Student extends Connection{

    public function allStudents(){

    $q = mysqli_query($this->conn,"SELECT * FROM student");
    ?>
    <table>
            <thead>
                <tr>										
                    <th data-toggle="true">Name</th>
                    <th data-toggle="true">Details</th>
                    <th data-toggle="true">Details Json</th>
                    <th data-toggle="true">Details XML</th>
                </tr>
            </thead>
        <tbody>
    <?php
    while ($rw=mysqli_fetch_object($q)) {
        ?>
           <tr>
            <td><?php echo "$rw->name"; ?></td>
            <td><a href="details.php?sid=<?php echo $rw->id;?>" >Details</td>
            <td><a href="details-json.php?sid=<?php echo $rw->id;?>" >Details Json</td>
            <td><a href="details-xml.php?sid=<?php echo $rw->id;?>" >Details Xml</td>
        <tr>
        <?php
        }

        ?>
            </tbody>			
	    </table>

        <?php
    } 
 
    public function detailStudent(){

    $studet_id=isset($_GET['sid']) && is_numeric($_GET['sid']) ? $_GET['sid']:1;
    $q=mysqli_query($this->conn,"SELECT * FROM marks WHERE student_id=$studet_id");
        ?>
        <table>
            <thead>
                <tr>										
                    <th data-toggle="true">Fizicko</th>
                    <th data-toggle="true">Muzicko</th>
                    <th data-toggle="true">Biologija</th>
                    <th data-toggle="true">Fizika</th>
                    <th data-toggle="true">Hemija</th>
                    <th data-toggle="true">Prosecna ocena je:</th>
                    <th data-toggle="true">Polozio:</th>
                </tr>
            </thead>
        <tbody>
        <?php
    while ($rw=mysqli_fetch_object($q)) {

    $array = array($rw->fizicko, $rw->muzicko, $rw->biologija, $rw->fizika, $rw->hemija);
    $average =  array_sum($array) / count($array);

    ?>
        <tr>
            <td><?php echo $rw->fizicko; ?></td>
            <td><?php echo $rw->muzicko; ?></td>
            <td><?php echo $rw->biologija; ?></td>
            <td><?php echo $rw->fizika; ?></td>
            <td><?php echo $rw->hemija; ?></td>
            <td><?php echo $average; ?></td>
            <td><?php if($average >= 7){
            ?>
                <p>Da</p>
            <?php
            }else{
            ?>
                <p>Ne</p>
            <?php
            } ?></td>
        <tr>
    <?php
        }
        ?>
            </tbody>			
	    </table>
        <a href="index.php">Back</a>
        <?php
    }
  
    public function prepareData(){
        $studet_id=isset($_GET['sid']) && is_numeric($_GET['sid']) ? $_GET['sid']:1;
        $q=mysqli_query($this->conn,"SELECT * FROM marks WHERE student_id=$studet_id");
        while ($rw=mysqli_fetch_object($q)) {
    
        $array = array($rw->fizicko, $rw->muzicko, $rw->biologija, $rw->fizika, $rw->hemija);
        $average =  array_sum($array) / count($array);
        
        $polozio = TRUE;
     
        if($average < 7 ){
            $polozio = FALSE;
        } 

        $data = [
            "student"=>$rw,
            "average"=>$average,
            "polozio"=>$polozio
          ];
 
        }
        return $data;
    }

    public function detailJson(){
        
        $dataFunction = $this->prepareData();
        echo json_encode($dataFunction);
 
    }

    public function detailXml(){
    
        $dataFunction = $this->prepareData();

        $xml = new SimpleXMLElement('');
        array_walk_recursive($array, array ($xml,'addChild'));
        print $xml->asXML();
        var_dump($dataFunction);
         
    }

    public function csm(){
        $polozili_id = [];
        $q = mysqli_query($this->conn,"SELECT * FROM marks");
        while($rw = mysqli_fetch_object($q)){
        
        $average = 0;
        $subjects = get_object_vars($rw);
       
        $properties = array_keys($subjects);

        $suma = 0;
        $counter = 0;
        
        for ($counterx = 0; $counterx < count(get_object_vars($rw)); $counterx++) {
               if($properties[$counterx] != 'id' && $properties[$counterx] !='student_id'){
                $suma+= $subjects[$properties[$counterx]];
                $counter+=1;
               }

             
        }
        $average = $suma / ($counter);
        
        
        if($average >= 7 ){
           array_push($polozili_id,$subjects[$properties[6]] );
        }
         
        }
          
        $qs = mysqli_query($this->conn,"SELECT * FROM student");

        $studenti = [];
        while ($rw=mysqli_fetch_object($qs)) {
            foreach($polozili_id as $id){
                if($rw->id == $id){
                    array_push($studenti,$rw );

                }
            }
           
        }
        echo json_encode($studenti);
    }


    public function csm2(){
        $polozili_id = [];
        $q = mysqli_query($this->conn,"SELECT * FROM marks");
        while($rw = mysqli_fetch_object($q)){
        
        $average = 0;
        $subjects = get_object_vars($rw);
       
        $properties = array_keys($subjects);

        $suma = 0;
        $counter = 0;
        
        for ($counterx = 0; $counterx < count(get_object_vars($rw)); $counterx++) {
               if($properties[$counterx] != 'id' && $properties[$counterx] !='student_id'){
                $suma+= $subjects[$properties[$counterx]];
                $counter+=1;
               }

             
        }
        $average = $suma / ($counter);
        
        
        if($average < 7 ){
           array_push($polozili_id,$subjects[$properties[6]] );
        }
         
        }
          
        $qs = mysqli_query($this->conn,"SELECT * FROM student");

        $studenti = [];
        while ($rw=mysqli_fetch_object($qs)) {
            foreach($polozili_id as $id){
                if($rw->id == $id){
                    array_push($studenti,$rw );

                }
            }
           
        }
        echo json_encode($studenti);
    }
    
   
    }

 